<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'afficher_logo_description' => 'Afficher un logo (d\'article, de rubrique, d\'auteur, du site...) dans un texte.',
	'afficher_logo_slogan' => 'Fais voir ton logo !',
);
